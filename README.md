# Photo Scripts

This repository contains scripts that I use to keep nice metadata embedded in
the EXIF tags of our digital photos. The nice thing about this is that it lets
me switch pretty easily between different photo hosting systems. I put the
metadata in the files originally when I took them off of Flickr, and then have
since migrated through first a statically hosted site solution based on Sigal,
and now use the photo support built into Nextcloud. All of those migrations were
painless, because the metadata was baked into the files.

- `clean-folder` --- remove a bunch of silly tags often set by digital cameras
  or cell phones, and set copyright and author tags
- `set-collection` --- set a "collection" or "album" tag
- `set-titles` --- show each of the photos in the folder, and prompt for titles
  and descriptions
- `rename-files` --- rename files based on dates and titles
- Other miscellaneous scripts:
  - `one-time/fix-charsets` --- convert old metadata character sets to UTF-8
  - `one-time/move-collections` --- move files into folders named based on the
    included album tags

## License

All scripts here are released under CC0.
